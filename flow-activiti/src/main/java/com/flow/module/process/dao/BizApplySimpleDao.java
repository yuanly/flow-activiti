package com.flow.module.process.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.flow.module.process.pojo.BizApplySimple;

/**
 * biz_apply_simple的Dao
 * @author autoCode
 * @date 2017-12-29 10:14:11
 * @version V1.0.0
 */
public interface BizApplySimpleDao {

	public abstract void save(BizApplySimple bizApplySimple);

	public abstract void update(BizApplySimple bizApplySimple);

	public abstract void delete(@Param("id")String id);

	public abstract BizApplySimple get(@Param("id")String id);

	public abstract List<BizApplySimple> findBizApplySimple(BizApplySimple bizApplySimple);
	
	public abstract int findBizApplySimpleCount(BizApplySimple bizApplySimple);

	public abstract void updateStatus(@Param("id")String id, @Param("status")Integer status);
}