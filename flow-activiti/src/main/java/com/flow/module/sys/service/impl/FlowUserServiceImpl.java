package com.flow.module.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.flow.module.sys.dao.FlowUserDao;
import com.flow.module.sys.pojo.FlowUser;
import com.flow.module.sys.service.FlowUserService;
import com.system.comm.model.Page;
import com.system.comm.utils.FrameNoUtil;
import com.system.comm.utils.FrameTimeUtil;
import com.system.handle.model.ResponseFrame;
import com.system.handle.model.ResponseCode;

/**
 * flow_user的Service
 * @author autoCode
 * @date 2017-12-29 10:12:19
 * @version V1.0.0
 */
@Component
public class FlowUserServiceImpl implements FlowUserService {

	@Autowired
	private FlowUserDao flowUserDao;
	
	@Override
	public ResponseFrame saveOrUpdate(FlowUser flowUser) {
		ResponseFrame frame = new ResponseFrame();
		FlowUser org = getBySysNoUserId(flowUser.getSysNo(), flowUser.getUserId());
		if(org == null) {
			flowUser.setId(FrameNoUtil.uuid());
			flowUser.setCreateTime(FrameTimeUtil.getTime());
			flowUserDao.save(flowUser);
		} else {
			flowUser.setId(org.getId());
			flowUserDao.update(flowUser);
		}
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public FlowUser getBySysNoUserId(String sysNo, String userId) {
		return flowUserDao.getBySysNoUserId(sysNo, userId);
	}

	@Override
	public FlowUser get(String id) {
		return flowUserDao.get(id);
	}

	@Override
	public ResponseFrame pageQuery(FlowUser flowUser) {
		flowUser.setDefPageSize();
		ResponseFrame frame = new ResponseFrame();
		int total = flowUserDao.findFlowUserCount(flowUser);
		List<FlowUser> data = null;
		if(total > 0) {
			data = flowUserDao.findFlowUser(flowUser);
		}
		Page<FlowUser> page = new Page<FlowUser>(flowUser.getPage(), flowUser.getSize(), total, data);
		frame.setBody(page);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}
	
	@Override
	public ResponseFrame delete(String id) {
		ResponseFrame frame = new ResponseFrame();
		flowUserDao.delete(id);
		frame.setCode(ResponseCode.SUCC.getCode());
		return frame;
	}

	@Override
	public String getId(String sysNo, String userId) {
		FlowUser user = getBySysNoUserId(sysNo, userId);
		return user != null ? user.getId() : null;
	}
}